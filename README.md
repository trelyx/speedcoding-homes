# Homes #

This is a simple home plugin.

## **Commands:** ##
* * /home - *- Teleports you to your main home*
* * /home <home> - *- Teleports you to your specified home*
* * /homes - *- Give you a list of all your homes*
* * /delhome - *- Removes your main home*
* * /delhome <home> - *- Removes your specified home*
* * /sethome - *- Creates your main home*
* * / sethome <home> - *- Creates your specified home*

## **Permissions:** ##
* * homes.* - *- Gives you access to every command!*
* * homes.home - *- Gives you access to /home and /home <home>*
* * homes.homes - *- Gives you access to /homes*
* * homes.delhome - *- Gives you access to /delhome and /delhome <home>*
* * homes.sethome - *- Gives you access to /sethome and /sethome <home>*

## **Found a bug/error, or just want to talk?** ##
* * Skype: neeh98
* * Mail: fabiansandberg98@hotmail.com

## **Social links:** ##
* * Twitter: www.twitter.com/Trelyx
* * Portfolio: www.behance.net/Trelyx
* * Youtube: www.youtube.com/imTrelyx
* * Soundcloud: www.soundcloud.com/fabianauu