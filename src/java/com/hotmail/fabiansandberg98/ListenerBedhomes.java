package com.hotmail.fabiansandberg98;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;

public class ListenerBedhomes implements Listener {
	private final homes plugin;

	public ListenerBedhomes(homes plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerBed(PlayerBedEnterEvent event) {
		Player player = event.getPlayer();
		if (plugin.isBed()) {
			if (plugin.getConfig().getString(
					"homes." + player.getUniqueId().toString()) != null) {
				if (!(plugin
						.getConfig()
						.getConfigurationSection(
								"homes." + player.getUniqueId()).getKeys(false)
						.size() >= plugin.maxHomes())) {
					plugin.setHome(player.getUniqueId().toString(), "home",
							player);

					player.setBedSpawnLocation(new Location(player.getWorld(),
							(int) player.getLocation().getX(), (int) player
									.getLocation().getY(), (int) player
									.getLocation().getZ(), player.getLocation()
									.getYaw(), player.getLocation().getPitch()));
				}
			}
		}
	}

}
