package com.hotmail.fabiansandberg98;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHomes implements CommandExecutor {
	private final homes plugin;

	public CommandHomes(homes plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (player.hasPermission("homes.homes")) {
				if (args.length == 0) {
					if (plugin.getConfig().getString(
							"homes." + player.getUniqueId().toString()) != null) {
						if (plugin
								.getConfig()
								.getConfigurationSection(
										"homes." + player.getUniqueId())
								.getKeys(false).size() != 0) {
							player.sendMessage(ChatColor.DARK_GREEN
									+ "Homes: "
									+ ChatColor.WHITE
									+ plugin.getListHomes(player.getUniqueId()
											.toString()));
							player.sendMessage(ChatColor.DARK_GREEN
									+ "You got "
									+ ChatColor.WHITE
									+ plugin.getConfig()
											.getConfigurationSection(
													"homes."
															+ player.getUniqueId())
											.getKeys(false).size()
									+ ChatColor.DARK_GREEN + " out of "
									+ ChatColor.WHITE + plugin.maxHomes()
									+ ChatColor.DARK_GREEN + " homes.");
							return true;
						} else {
							player.sendMessage(ChatColor.DARK_RED
									+ "You don't have any homes!");
							return true;
						}
					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "You don't have any homes!");
						return true;
					}
				} else if (args.length == 1) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too many arguments!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED
						+ "You do not have permission for that command!");
				return true;
			}
		} else {
			sender.sendMessage("This command can only be used in-game!");
			return true;
		}
		return true;
	}

}
