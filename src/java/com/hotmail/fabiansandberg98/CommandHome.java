package com.hotmail.fabiansandberg98;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHome implements CommandExecutor {
	private final homes plugin;

	public CommandHome(homes plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (player.hasPermission("homes.home")) {
				if (args.length == 0 || args[0].equalsIgnoreCase("home")) {
					if (plugin.getConfig().getString(
							"homes." + player.getUniqueId().toString()) != null) {
						if (plugin.getHome(player.getUniqueId().toString(),
								"home") != null) {
							player.sendMessage(ChatColor.DARK_GREEN
									+ "You got teleported to your main home!");
							World world = Bukkit.getWorld(getWorld(player
									.getUniqueId().toString(), "home"));
							plugin.getLocation(
									player,
									world,
									getX(player.getUniqueId().toString(),
											"home"),
									getY(player.getUniqueId().toString(),
											"home"),
									getZ(player.getUniqueId().toString(),
											"home"),
									getYa(player.getUniqueId().toString(),
											"home"),
									getPi(player.getUniqueId().toString(),
											"home"));
							return true;

						} else {
							player.sendMessage(ChatColor.DARK_RED
									+ "You don't have a main home!");
							return true;
						}
					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "You don't have any homes!");
						return true;
					}
				} else if (args.length == 1) {
					if (plugin.isAlphanumeric(args[0])) {
						if (plugin.getConfig().getString(
								"homes." + player.getUniqueId().toString()) != null) {
							if (plugin.getHome(player.getUniqueId().toString(),
									args[0]) != null) {
								player.sendMessage(ChatColor.DARK_GREEN
										+ "You got teleported to your home!");
								World world = Bukkit.getWorld(getWorld(player
										.getUniqueId().toString(), args[0]));
								plugin.getLocation(
										player,
										world,
										getX(player.getUniqueId().toString(),
												args[0]),
										getY(player.getUniqueId().toString(),
												args[0]),
										getZ(player.getUniqueId().toString(),
												args[0]),
										getYa(player.getUniqueId().toString(),
												args[0]),
										getPi(player.getUniqueId().toString(),
												args[0]));
								return true;

							} else {
								player.sendMessage(ChatColor.DARK_RED
										+ "That home does not exist!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.DARK_RED
									+ "You don't have any homes!");
							return true;
						}
					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "You can only use alphanumeric characters!");
						return true;
					}
				} else if (args.length > 1) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too many arguments!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED
						+ "You do not have permission to use that command!");
				return true;
			}
		} else {
			sender.sendMessage("This command can only be used in-game!");
			return true;
		}
		return false;
	}

	public String getWorld(String uuid, String args) {
		return plugin.getConfig().getString(
				"homes." + uuid + "." + args.toLowerCase() + ".world");
	}

	public int getX(String uuid, String args) {
		return plugin.getConfig().getInt(
				"homes." + uuid + "." + args.toLowerCase() + ".locx");
	}

	public int getY(String uuid, String args) {
		return plugin.getConfig().getInt(
				"homes." + uuid + "." + args.toLowerCase() + ".locy");
	}

	public int getZ(String uuid, String args) {
		return plugin.getConfig().getInt(
				"homes." + uuid + "." + args.toLowerCase() + ".locz");
	}

	public float getYa(String uuid, String args) {
		return plugin.getConfig().getInt(
				"homes." + uuid + "." + args.toLowerCase() + ".floYa");
	}

	public float getPi(String uuid, String args) {
		return plugin.getConfig().getInt(
				"homes." + uuid + "." + args.toLowerCase() + ".floPi");
	}
}
