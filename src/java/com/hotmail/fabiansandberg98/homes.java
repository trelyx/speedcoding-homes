package com.hotmail.fabiansandberg98;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class homes extends JavaPlugin {

	@Override
	public void onEnable() {
		getLogger().info("onEnable has started!");

		this.saveDefaultConfig();
		this.getConfig().options().copyDefaults(true);

		this.getCommands();
		this.getServer().getPluginManager()
				.registerEvents(new ListenerBedhomes(this), this);
		getLogger().info("Homes has been enabled!");
	}

	@Override
	public void onDisable() {
		getLogger().info("onDisable has started!");

		getLogger().info("Homes has been disabled!");
	}

	public void getCommands() {
		// COMMAND home
		CommandHome home = new CommandHome(this);
		getCommand("home").setExecutor(home);

		// COMMAND homes
		CommandHomes cHomes = new CommandHomes(this);
		getCommand("homes").setExecutor(cHomes);

		// COMMAND delhome
		CommandDelhome delhome = new CommandDelhome(this);
		getCommand("delhome").setExecutor(delhome);

		// COMMAND sethome
		CommandSethome sethome = new CommandSethome(this);
		getCommand("sethome").setExecutor(sethome);
	}

	/*
	 * Get home from config
	 */
	public String getHome(String uuid, String args) {
		return getConfig()
				.getString("homes." + uuid + "." + args.toLowerCase());
	}

	/*
	 * List of every home for the player
	 */
	public String getListHomes(String uuid) {
		return getConfig().getConfigurationSection("homes." + uuid)
				.getKeys(false).toString().replace("]", "").replace("[", "");
	}

	/*
	 * Set home
	 */
	public void setHome(String uuid, String args, Player player) {
		try {
			getConfig().set(
					"homes." + uuid + "." + args.toLowerCase() + ".world",
					player.getLocation().getWorld().getName());
			getConfig().set(
					"homes." + uuid + "." + args.toLowerCase() + ".locx",
					player.getLocation().getX());
			getConfig().set(
					"homes." + uuid + "." + args.toLowerCase() + ".locy",
					player.getLocation().getY());
			getConfig().set(
					"homes." + uuid + "." + args.toLowerCase() + ".locz",
					player.getLocation().getZ());
			getConfig().set(
					"homes." + uuid + "." + args.toLowerCase() + ".floYa",
					player.getLocation().getYaw());
			getConfig().set(
					"homes." + uuid + "." + args.toLowerCase() + ".floPi",
					player.getLocation().getPitch());
			saveConfig();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Del home
	 */
	public void delHome(String uuid, String args) {
		try {
			getConfig().set("homes." + uuid + "." + args.toLowerCase(), null);
			saveConfig();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Home location
	 */
	public boolean getLocation(Player player, World world, int x, int y, int z,
			float ya, float pi) {
		return player.teleport(new Location(world, x, y, z, ya, pi));
	}

	/*
	 * Max homes
	 */
	public int maxHomes() {
		return getConfig().getInt("settings.max_homes");
	}

	/*
	 * Bed isBed
	 */
	public boolean isBed() {
		return getConfig().getBoolean("settings.bed_home");
	}

	public boolean isAlphanumeric(String string) {
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c < 0x30 || (c >= 0x3a && c <= 0x40) || (c > 0x5a && c <= 0x60)
					|| c > 0x7a)
				return false;
		}
		return true;
	}
}
