package com.hotmail.fabiansandberg98;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandDelhome implements CommandExecutor {
	private final homes plugin;

	public CommandDelhome(homes plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (player.hasPermission("homes.delhome")) {
				if (args.length == 0) {

					if (plugin.getConfig().getString(
							"homes." + player.getUniqueId().toString()) != null) {
						if (plugin.getHome(player.getUniqueId().toString(),
								"home") != null) {
							player.sendMessage(ChatColor.DARK_GREEN
									+ "You deleted your main home!");
							plugin.delHome(player.getUniqueId().toString(),
									"home");
							if (plugin.isBed()) {
								player.setBedSpawnLocation(player.getLocation()
										.getWorld().getSpawnLocation());
								return false;
							}
							return true;

						} else {
							player.sendMessage(ChatColor.DARK_RED
									+ "You don't have a main home!");
							return true;
						}

					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "You don't have any homes!");
						return true;
					}
				} else if (args.length == 1) {

					if (plugin.isAlphanumeric(args[0])) {
						if (plugin.getConfig().getString(
								"homes." + player.getUniqueId().toString()) != null) {
							if (plugin.getHome(player.getUniqueId().toString(),
									args[0]) != null) {
								player.sendMessage(ChatColor.DARK_GREEN
										+ "You deleted one of your homes!");
								plugin.delHome(player.getUniqueId().toString(),
										args[0]);
								return true;

							} else {
								player.sendMessage(ChatColor.DARK_RED
										+ "That home does not exist!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.DARK_RED
									+ "You do not have any homes!");
							return true;
						}

					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "You can only use alphanumeric characters!");
						return true;
					}
				} else if (args.length > 1) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too many arguments!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED
						+ "You do not have permission to use that command!");
				return true;
			}
		} else {
			sender.sendMessage("This command can only be used in-game!");
			return true;
		}
		return false;
	}
}