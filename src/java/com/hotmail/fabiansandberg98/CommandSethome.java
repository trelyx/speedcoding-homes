package com.hotmail.fabiansandberg98;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSethome implements CommandExecutor {
	private final homes plugin;

	public CommandSethome(homes plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (player.hasPermission("homes.sethome")) {
				if (args.length == 0 || args[0].equalsIgnoreCase("home")) {

					if (plugin.getConfig().getString(
							"homes." + player.getUniqueId().toString()) != null) {
						if (!(plugin
								.getConfig()
								.getConfigurationSection(
										"homes." + player.getUniqueId())
								.getKeys(false).size() >= plugin.maxHomes())) {
							player.sendMessage(ChatColor.DARK_GREEN
									+ "You created a main home!");
							plugin.setHome(player.getUniqueId().toString(),
									"home", player);
							if (plugin.isBed()) {
								player.setBedSpawnLocation(new Location(player
										.getWorld(), (int) player.getLocation()
										.getX(), (int) player.getLocation()
										.getY(), (int) player.getLocation()
										.getZ(), player.getLocation().getYaw(),
										player.getLocation().getPitch()));
								return false;
							}
							return true;

						} else {
							player.sendMessage(ChatColor.DARK_RED
									+ "You got too many homes!");
							return true;
						}

					} else {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You created a new home!");
						plugin.setHome(player.getUniqueId().toString(), "home",
								player);
						return true;
					}
				} else if (args.length == 1) {

					if (plugin.isAlphanumeric(args[0])) {
						if (plugin.getConfig().getString(
								"homes." + player.getUniqueId().toString()) != null) {
							if (!(plugin
									.getConfig()
									.getConfigurationSection(
											"homes." + player.getUniqueId())
									.getKeys(false).size() >= plugin.maxHomes())) {
								player.sendMessage(ChatColor.DARK_GREEN
										+ "You created a new home!");
								plugin.setHome(player.getUniqueId().toString(),
										args[0], player);
								return true;

							} else {
								player.sendMessage(ChatColor.DARK_RED
										+ "You got too many homes!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.DARK_GREEN
									+ "You created a new home!");
							plugin.setHome(player.getUniqueId().toString(),
									args[0], player);
							return true;
						}

					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "You can only use alphanumeric characters!");
						return true;
					}
				} else if (args.length > 1) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too many arguments!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED
						+ "You do not have permission to use that command!");
				return true;
			}
		} else {
			sender.sendMessage("This command can only be used in-game!");
			return true;
		}
		return false;
	}
}
